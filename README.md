# Project description

A utility application for the mandatory glass fragmentation test in glass tempering industry. The application combines artificial intelligence (AI) and augmented reality (AR) to analyze glass fragmentation pattern in an image. Both European (EN) and American (ANSI) standards are supported.

The application allows you to store the results for later inspection. You can also add in important information, such as order number and glass thickness for tracking purposes. Original image and the AI analysis can be viewed to verify the accuracy of the analysis.

https://play.google.com/store/apps/details?id=com.glaston.siru&hl=en

# My role in development and technologies used

- This was my first Android development project ever in April of 2019
- I was responsible for most of the UI implementation, making sure it followed the UI spec to pixel
- Those views in the screenshots were mostly implemented by me
- Experience from Material-UI components
- Most of my allocated time was spent configuring styles.xml and writing UI logic with Java
- The machine vision component and backend service was developed by a third party
